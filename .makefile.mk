SRC:=src
OUT:=out
REPORT?=report

.SECONDARY:
.PHONY: all doc handout clean _%_dep _%
ALL:=$(SOURCES:%=$(OUT)/%.mat)
all: $(ALL)

doc: $(OUT)/$(REPORT).pdf

handout: $(PROJECT).zip

clean:
	@rm -f $(SOURCES:%=$(OUT)/%.mat) $(SOURCES:%=$(OUT)/%.out) $(SOURCES:%=$(OUT)/%.m)

$(OUT):
	@mkdir -p $@

$(OUT)/%.mat $(OUT)/%.out: $(OUT)/%.m _%_dep
	@cd $(OUT) && octave --no-gui $*.m | sed -f ../bin/outpp.sed | tee $*.out
	@test -f $(OUT)/$*.mat

$(OUT)/%.m: $(SRC)/%.m $(OUT)
	@echo "PS4 '+'; echo on" > $@
	@cat $< >> $@

$(OUT)/%.pdf: $(OUT)/%.rst _%_dep
	@cd $(OUT) && pandoc -i $*.rst -o $*.pdf

$(OUT)/%.rst: $(SRC)/%.rst $(ALL)
	@cat $< | { cd $(OUT) && ../bin/rstpp.py; } > $@

$(PROJECT).zip: $(OUT)/$(REPORT).pdf
	zip $@ $(ARCHIVE) $< $(wildcard $(SRC)/*.*)

_%_dep:
	

_%: $(OUT)/%.mat
	

