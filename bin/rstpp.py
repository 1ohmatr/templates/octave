#!/usr/bin/env python3
from sys import stdin,stdout
from io import StringIO
import re

_include_re = re.compile(r'^.*\.\.\s+include::\s+(?P<filename>.*)\s*$')

def process(line,out=None):
    if out is None:
        out = StringIO()
    match = _include_re.match(line)
    if match:
        filename = match.group('filename')
        out.write('.. code:: octave\n\n')
        with open(filename,'r+') as f:
            out.write('    ')
            out.write(f.read().replace('\n','\n    '))
        out.write('\n')
    else:
        out.write(line)
    return out

if __name__=='__main__':
    for line in stdin.readlines():
        process(line,out=stdout)
